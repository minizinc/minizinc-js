import {createWrapper} from 'emscripten_wrapper';
import {initWrapper} from './internal/index.mjs';

export const MINIZINC = initWrapper(createWrapper({
  module: () => import('./emscripten/minizinc.js'),
  wasm: () => import('./emscripten/minizinc.wasm'),
  data: () => import('./emscripten/minizinc.data')
}));

export default MINIZINC;
