var createWrapper = require('emscripten_wrapper').createWrapper;

var wrapper = createWrapper(() => Promise.resolve(require('./emscripten/mzn2doc')));
// set the virtual shared library path
wrapper.environmentVariables.MZN_STDLIB_DIR = '/minizinc';
// MZN_SOLVER_PATH

module.exports = wrapper;
