import {IMiniZincModule} from "./internal/types";
export * from "./internal/types";

export const MINIZINC: IMiniZincModule;

export default MINIZINC;
