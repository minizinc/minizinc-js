var createWrapper = require('emscripten_wrapper').createWrapper;
var initWrapper = require('./internal').initWrapper;

var wrapper = initWrapper(createWrapper(() => Promise.resolve(require('./emscripten/minizinc_asm'))));

module.exports = wrapper;
