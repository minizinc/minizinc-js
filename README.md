minizinc-js
===========
[![License: MPL-2.0][mpl2-image]][mpl2-url] [![NPM version][npm-image]][npm-url]

This is the npm package for the [Emscripten](https://emscripten.org) version of [MiniZinc](http://minizinc.org) wrapped using [emscripten_wrapper](https://github.com/sgratzl/emscripten_wrapper).

Usage
-----

Best used as part of [MiniZinc Packages](https://gitlab.com/minizinc/minizinc-webide).


Development Environment
-----------------------

**Installation**

```bash
git clone https://gitlab.com/minizinc/minizinc-js.git
cd minizinc-js
npm install
```

**Build distribution packages**

```bash
npm run build
```

**Release distribution packages**

based on [release-it](https://github.com/release-it/release-it)

```bash
npm run release:patch
```

[mpl2-image]: https://img.shields.io/badge/License-MPL%202.0-yellow.svg
[mpl2-url]: https://opensource.org/licenses/MPL-2.0
[npm-image]: https://badge.fury.io/js/minizinc-js.svg
[npm-url]: https://npmjs.org/package/minizinc-js
