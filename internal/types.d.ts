import {IAsyncEMWMainWrapper, IEMWWorkerClient, ISyncEMWWrapper, IEMWMain, IEMWMainPromise, IWorkerLike} from "emscripten_wrapper";

export interface ISolverMscFile {
  id: string;
  name: string;
  description: string;
  stdFlags: string[];
  version: string;

  executable?: string;
  tags?: string[];
  mznlib?: string;

  supportsMzn?: boolean;
  supportsFzn?: boolean;
  needsSolns2Out?: boolean;
  needsMznExecutable?: boolean;
  needsStdlibDir?: boolean;
  isGUIApplication?: boolean;
}

export interface ISolverInfo {
  SOLVER_ID: string;
  SOLVER_MSC_FILE: ISolverMscFile;
  SOLVER_BUILTIN_DIR?: string;
  SOLVER_BUILTIN_FILES?: {[name: string]: string};
}

export declare type ISolverModule = IAsyncEMWMainWrapper<{}> & ISolverInfo;
export declare type ISolverWorkerClient = IEMWWorkerClient<{}> & IEMWMainPromise & ISolverInfo;

export interface IModuleExtensions {
  registerSolver(name: string, mscFile: ISolverMscFile, builtinDir?: string, builtinFiles?: {[name: string]: string}): Promise<any>;
  registerSolver(solver: ISolverInfo): Promise<any>;
  chooseSolver<T extends ISolverInfo>(solvers: T[], solver?: string): T | null;
}

export declare type IMiniZincWorkerClient = IEMWWorkerClient<{}> & IEMWMainPromise & IModuleExtensions;

export interface IExtendedWrapper extends IModuleExtensions {
  createWorkerClient(worker: IWorkerLike): IMiniZincWorkerClient;
}

export declare type IMiniZincModule = IAsyncEMWMainWrapper<{}> & IExtendedWrapper;
export declare type IMiniZincSyncModule = ISyncEMWWrapper<{}> & IEMWMain;

