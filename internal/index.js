module.exports.initWrapper = function(wrapper) {
  // set the virtual shared library path
  wrapper.environmentVariables.MZN_STDLIB_DIR = '/minizinc/minizinc';
  wrapper.environmentVariables.MZN_SOLVER_PATH = '/minizinc/solvers';

  wrapper.registerSolver = function (nameOrSolver, mscFile, builtinDir, builtinFiles) {
    var name = nameOrSolver;
    if (typeof nameOrSolver !== 'string') {
      name = nameOrSolver.SOLVER_ID;
      mscFile = nameOrSolver.SOLVER_MSC_FILE;
      builtinDir = nameOrSolver.SOLVER_BUILTIN_DIR;
      builtinFiles = nameOrSolver.SOLVER_BUILTIN_FILES;
    }

    wrapper.on('ready', function (instance) {
      var fs = instance.fileSystem;
      try {
        fs.mkdir('/minizinc/solvers');
      } catch (e) {
        if (e && e.errno !== 17) {
          console.warn('cannot create solver dir', e);
        }
      }
      fs.writeFile(`/minizinc/solvers/${name}.msc`, typeof mscFile === 'string' ? mscFile : JSON.stringify(mscFile, null, 2), {
        encoding: 'utf8'
      });
      if (!builtinDir) {
        return;
      }
      try {
        fs.mkdir(`/minizinc/minizinc/${builtinDir}`);
        Object.keys(builtinFiles || {}).forEach(function (key) {
          fs.writeFile(`/minizinc/minizinc/${builtinDir}/${key}`, builtinFiles[key], {
            encoding: 'utf8'
          });
        });
      } catch (e) {
        if (e && e.errno !== 17) {
          console.warn('cannot create built in dir', e);
        }
      }
    });
  };

  function chooseSolver(solvers, solver) {
    if (!solver) {
      return null;
    }
    return solvers.find((s) => {
      if (solver === s.SOLVER_ID) {
        return true;
      }
      if (solver.SOLVER_MSC_FILE && solver.SOLVER_MSC_FILE.id === solver) {
        return true;
      }
      return false;
    });
  }

  var superCreateWorkerClient = wrapper.createWorkerClient;
  wrapper.createWorkerClient = function () {
    var r = superCreateWorkerClient.apply(this, Array.from(arguments));

    // inject the registerSolver method
    r.registerSolver = function (nameOrSolver, mscFile, builtinDir, builtinFiles) {
      var name = nameOrSolver;
      if (typeof nameOrSolver !== 'string') {
        name = nameOrSolver.SOLVER_ID;
        mscFile = nameOrSolver.SOLVER_MSC_FILE;
        builtinDir = nameOrSolver.SOLVER_BUILTIN_DIR;
        builtinFiles = nameOrSolver.SOLVER_BUILTIN_FILES;
      }
      return this.sendAndWaitOK({type: 'registerSolver', name: name, mscFile: mscFile, builtinDir: builtinDir, builtinFiles: builtinFiles});
    };
    r.chooseSolver = chooseSolver;

    return r;
  };

  var superCreateWorker = wrapper.createWorker;
  wrapper.createWorker = function () {
    var r = superCreateWorker.apply(this, Array.from(arguments));

    // inject the registerSolver handle method
    const superHandleMessage = r.handleMessage;
    r.handleMessage = function (msg, reply) {
      if (msg.type === 'registerSolver') {
        this.module.registerSolver(msg.name, msg.mscFile, msg.builtinDir, msg.builtinFiles);
        return reply({key: msg.key, type: 'ok'});
      }
      return superHandleMessage.call(this, msg, reply);
    };

    return r;
  };

  wrapper.chooseSolver = chooseSolver;

  return wrapper;
}
