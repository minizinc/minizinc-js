import createWrapper from 'emscripten_wrapper';

const MZN2DOC = createWrapper(() => import('./emscripten/mzn2doc_asm'));
MZN2DOC.environmentVariables.MZN_STDLIB_DIR = '/minizinc';
// MZN_SOLVER_PATH

export default MZN2DOC;
