import {createWrapper} from 'emscripten_wrapper';
import {initWrapper} from './internal/index.mjs';

const MINIZINC = initWrapper(createWrapper(() => import('./emscripten/minizinc_asm')));

export default MINIZINC;
