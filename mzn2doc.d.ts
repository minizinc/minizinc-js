import {IAsyncEMWMainWrapper, ISyncEMWWrapper, IEMWMain} from "emscripten_wrapper";

export declare type IMzn2DocModule = IAsyncEMWMainWrapper<{}>;
export declare type IMzn2DocSyncModule = ISyncEMWWrapper<{}> & IEMWMain;

export const MZN2DOC: IMzn2DocModule;

export default MZN2DOC;
