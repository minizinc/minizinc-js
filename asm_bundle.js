import {createWrapper} from 'emscripten_wrapper';
import {initWrapper} from './internal/index.mjs';

export const MINIZINC = initWrapper(createWrapper({
  module: () => import('./emscripten/minizinc_asm.js'),
  data: () => import('./emscripten/minizinc.data'),
  mem: () => import('./emscripten/minizinc_asm.js.mem')
}));

export default MINIZINC;
